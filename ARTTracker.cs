﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
Modified by prasad

namespace EntityModel
{
    public class ARTTracker
    {
        public int No_Of_New_Cases_current_month { get; set; }
        public int NO_of_cases_carried_forward { get; set; }

        public int No_of_Cases_Contacted_through_Outreach { get; set; }

        public int Agreed_to_Visit_ART_Centre { get; set; }

        public int Physically_taken_to_ART_Centre { get; set; }

        public int Death { get; set; }

        public int Taking_ART_at_Other_NACO_ART_center { get; set; }

        public int Opted_Out_of_the_Program { get; set; }

        public int Incorrect_Address { get; set; }

        public int Migrated { get; set; }

        public int Transferred_Out { get; set; }

        public int Alternate_Medicine { get; set; }

        public int Taking_Private_ART_treatment { get; set; }

        public int Others { get; set; }

        public int No_Of_Cases_Reported_back_t_ART_Centre { get; set; }

        
    }
}
